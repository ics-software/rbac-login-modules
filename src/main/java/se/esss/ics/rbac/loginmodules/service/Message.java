/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 * 
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules.service;

/**
 * 
 * <code>Message</code> is a wrapper around a string message, which represents the result of a certain action. The
 * message contains the message and a flag stating if the message is the result of a successful or unsuccessful action.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
public class Message {

    private final String message;
    private final boolean successful;

    /**
     * Constructs a new message.
     * 
     * @param message the message
     * @param successful true if the message is a result of a successful action or false otherwise
     */
    public Message(String message, boolean successful) {
        this.message = message;
        this.successful = successful;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return true if this message is a result of a successful action or false otherwise
     */
    public boolean isSuccessful() {
        return successful;
    }
}
