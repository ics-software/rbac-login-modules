/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.io.Serializable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeListener;
import se.esss.ics.rbac.access.Token;

import com.google.common.collect.ImmutableSet;

/**
 * This handles binding of the {@link SecurityFacade} to HTTP session. At bind time the facade is constructed and
 * destroyed at unbind time.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class SecurityFacadeBinder implements HttpSessionBindingListener, SecurityFacadeListener, Serializable {

    private static final long serialVersionUID = 8446801570889457982L;

    private static final Logger LOGGER = Logger.getLogger(SecurityFacadeBinder.class.getName());

    private ISecurityFacade securityFacade;
    private ImmutableSet<String> grantedPermissions;

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Binding security facade.");
        }
        securityFacade = SecurityFacade.newInstance();
        securityFacade.addSecurityFacadeListener(this);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Security Facade instance: " + securityFacade);
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Unbinding security facade.");
        }
        securityFacade.removeSecurityFacadeListener(this);
        securityFacade.destroy();
        grantedPermissions = null;
        securityFacade = null;
    }

    /** @return the security facade that is bound, or null if nothing bound */
    public ISecurityFacade getSecurityFacade() {
        return securityFacade;
    }

    /**
     * @return the set of cached granted permissions
     */
    public Set<String> getGrantedPermissions() {
        return grantedPermissions;
    }

    /**
     * Caches granted permissions.
     *
     * @param permissions the permissions names that are granted to the logged in user
     */
    public void setGrantedPermissions(Set<String> permissions) {
        if (permissions == null) {
            grantedPermissions = null;
        } else {
            grantedPermissions = ImmutableSet.copyOf(permissions);
        }
    }

    /*
     * (non-Javadoc)
     * @see se.esss.ics.rbac.access.SecurityFacadeListener#loggedIn(se.esss.ics.rbac.access.Token)
     */
    @Override
    public void loggedIn(Token token) {
    }

    /*
     * (non-Javadoc)
     * @see se.esss.ics.rbac.access.SecurityFacadeListener#loggedOut(se.esss.ics.rbac.access.Token)
     */
    @Override
    public void loggedOut(Token token) {
        grantedPermissions = null;
    }
}
